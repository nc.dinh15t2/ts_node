import express, { Application } from 'express';
import dotenv from 'dotenv';
import userRouter from './route/user.route';
import authRouter from './route/auth.route';
import { exceptionHandler, authenticationHandler } from './route/middleware';

dotenv.config();

const app: Application = express();
app.use(express.json());
app.use(authenticationHandler);

app.use('/api/users', userRouter);
app.use('/api/auth', authRouter);

app.use(exceptionHandler);

const port = process.env.PORT || 8000;

const server = app.listen(port, () => {
  console.log('listening', port);
});