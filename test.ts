import BaseDB from './dao/connect';
import { Document, Connection, Model, Schema, CreateQuery } from 'mongoose';
import user from './model/user';

interface IUser extends Document {
  username: string;
  password: string;
}

class UserDB extends BaseDB<IUser> {
  initModel(connection: Connection): Model<IUser> {
    const UserSchema: Schema = new Schema({
      username: String,
      password: String
    }, {collection: 'users'});
    return connection.model<IUser>('User', UserSchema);
  }

  public createUser({username, password}: CreateQuery<IUser>): Promise<IUser> {
    return this.model.create({username, password})
      .then((user: IUser) => {
        console.log(user);
        return user;
      });
  }
}

new UserDB().createUser({username: 'dinh', password: '466'});