import { Model } from 'objection';
import Knex from 'knex';
const knex = Knex({
  client: 'mysql',
  useNullAsDefault: true,
  connection: {
    host: 'localhost',
    port: 8006,
    user: 'dinh',
    password: '123',
    database: 'auth_db'
  },
  debug: true
});

export default knex;