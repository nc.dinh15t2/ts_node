import mongoose, {Connection, Document, Model} from 'mongoose';

class MongoDB {
  private _connection: Connection;
  constructor() {
    this._connect();
  }

  _connect(): void {
    this._connection = mongoose.createConnection('mongodb://localhost:8001',
    {
      user: 'dinh',
      pass: '123',
      dbName: 'auth_db',
      useNewUrlParser: true,
      useUnifiedTopology: true,
      poolSize:5
    });
  }

  get connection(): Connection {
    return this._connection;
  }
}

const mongoInstance = new MongoDB();

export default abstract class BaseDB <T extends Document> {
  private _model: Model<T>;
  constructor() {
    this._init();
  }
  protected abstract initModel(connection: Connection): Model<T>;
  _init(): void {
    this._model = this.initModel(mongoInstance.connection);
  }

  get model(): Model<T> {
    return this._model;
  }
}