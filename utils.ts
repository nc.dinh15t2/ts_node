import { Payload, Token } from './model/token.model';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

const CHARS: string = 'abcdefghijklmnopqrstuvwxyz0123456789';
export function getRandomString(length: number): string {
  const result = [];
  for (let i = 0; i < length; i++)
    result.push(CHARS[Math.floor(Math.random() * CHARS.length)]);
  return result.join('');
}

export function generateToken(payload: Payload): Token {
  return { 
    access_token: jwt.sign(payload, process.env.ACCESS_TOKEN_SALT, 
            { expiresIn: process.env.ACCESS_TOKEN_EXP + "d" }), 
    expired: new Date().getTime() + +process.env.ACCESS_TOKEN_EXP*86400000
  };
}

export function verifyToken(token: string) {
  return jwt.verify(token, process.env.ACCESS_TOKEN_SALT);
}

export function encodePassword(password: string): Promise<string> {
  return bcrypt.hash(password, 12);
}

export function comparePassword(password: string, encodePassword): Promise<boolean> {
  return bcrypt.compare(password, encodePassword);
}