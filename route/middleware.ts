import { validationResult, ValidationError, Result } from 'express-validator';
import { Request, Response, NextFunction } from 'express';
import { ValidationChain } from 'express-validator';
import { BusinessError, ForbiddenError, UnauthorizedError } from '../errors';
import { Payload } from '../model/token.model';
import { verifyToken } from '../utils';
import { User } from '../model/user.model';

export function validator (validationChains: ValidationChain[]) {
  return async (req: Request, res: Response, next: NextFunction) => {
    await Promise.all(validationChains.map(validationChain => validationChain.run(req)));
    const errors: Result<ValidationError> = validationResult(req);
    if(errors.isEmpty()) {
      return next();
    }
    return res.status(400).json(errors);
  }
}

export async function authenticationHandler(req: Request, res: Response, next: NextFunction) {
  const authHeader: string = req.headers.authorization;
  req.user = null;
  if(!authHeader) return next();
  const auth = authHeader.trim().split(' ');
  if(auth.length !== 2 || auth[0].toLowerCase() != 'bearer') 
    return next(new UnauthorizedError());
  const payload: Payload = <Payload> verifyToken(auth[1]);
  const user = await User.query().findById(payload.id);
  if (!user)
    return next(new UnauthorizedError());
  req.user = user;
  next();
}

export function authenticated(req: Request, res: Response, next: NextFunction) {
  if (!req.user)
    return next(new ForbiddenError());
  next();
}

export function exceptionHandler(error: Error, req, res: Response, next: NextFunction) {
  if (error instanceof BusinessError) {
    return res.json(error.getResponse());
  }
  console.log(error);
  res.json({
    status: 500,
    message: 'Unknown',
    error: error
  });
}