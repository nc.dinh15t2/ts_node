import { Router, Request, Response, NextFunction, response, request } from 'express';
import * as UserController from '../controller/user.controller';
import { User } from '../model/user.model';
import { body } from 'express-validator';
import { validator } from './middleware';
import { authenticated } from './middleware';
import { ResponseFactory } from '../model/reponse.model';

const router = Router();

const registerValidator = [
  body('email').notEmpty().withMessage('email cannot be empty')
    .isEmail().withMessage('email not valid'),
  body('username').notEmpty().withMessage('username cannot be empty')
    .isString().isLength({ min: 5, max: 100 }).withMessage('username must be string length [5..100]'),
  body('name').isObject({ strict: true }).withMessage('name is not valid'),
  body('name.last').isString().isLength({ min: 3, max: 100 }).withMessage('last namme must be string with length [3..100]'),
  body('name.first').optional().isString().isLength({ min: 3, max: 100 }).withMessage('first name must be string withh length [3: 100]')
];

router.post('/', validator(registerValidator),
  async (req: Request, res: Response, next: NextFunction) => {
    const user: User = req.body;
    try {
      const newUser: User = await UserController.register(user);
      res.json({ message: 'ok', data: newUser });
    } catch (err) {
      return next(err);
    }
  });

router.get('/', authenticated, async (req: Request, res: Response, next: NextFunction) => {
  const users = await UserController.getAll(req.query, req.user.id);
  res.json(users);
});

router.get('/:user_id', authenticated, async (req: Request, res: Response, next: NextFunction) => {
  let userId: string = req.params['user_id'];
  if (userId == 'me') userId = req.user.id;
  const user = await UserController.getById(userId, req.user.id);
  res.json(ResponseFactory.ok(user));
});

router.put('/:user_id', authenticated, async (req: Request, res: Response, next: NextFunction) => {
  let userId: string = req.params['user_id'];
  if (userId == 'me') userId = req.user.id;
  let user: User = await UserController.update(userId, req.body, req.user.id);
  res.json(ResponseFactory.ok(user));
});

export default router;