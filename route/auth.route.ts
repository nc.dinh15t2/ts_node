import { NextFunction, Router, Request, Response } from 'express';
import { validator } from './middleware';
import { body } from 'express-validator';
import { AuthController } from '../controller/auth.controller';
import { Token } from '../model/token.model';
import { BaseResponse } from '../model/reponse.model';

const router = Router();

const loginValidators = [
  body('username').notEmpty().isString().withMessage('username must be a string'),
  body('password').notEmpty().isString().withMessage('password must be a string')
];

router.post('/login', validator(loginValidators), 
    async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token: Token = await AuthController.login(req.body.username, req.body.password);
    res.json({
      status: 200,
      message: 'success',
      data: token
    });
  } catch(err) {
    next(err);
  }
});

export default router;