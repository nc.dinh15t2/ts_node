import knex from '../dao/mysql.dao';
import { JSONSchema, Model, QueryContext } from 'objection';
import { v4 } from 'uuid';

export enum Gender {
  MALE, FEMALE, UNKNOWN
};

export interface User {
  readonly id: v4;
  username: string;
  email: string;
  password?: string;
  name: { first?: string, last: string };
  gender: Gender;
  roles: string[],
  created?: number;
  modified?: number;
}

export class User extends Model {
  static tableName = 'users';
  static idColumn = 'id';

  static get jsonSchema(): JSONSchema {
    return {
      type: 'object',
      required: ['id', 'username', 'email', 'name'],
      properties: {
        id: {
          type: 'uuid',
          readOnly: true,
          default: v4()
        },
        name: {
          type: 'object',
          required: ['last'],
          properties: {
            first: { type: 'string' },
            last: { type: 'string' }
          }
        },
        roles: {
          type: 'array'
        }
      }
    }
  }

  async $beforeInsert(queryContext: QueryContext) {
    this.created = new Date().getTime();
  }
};

User.knex(knex);