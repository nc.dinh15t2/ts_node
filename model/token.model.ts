export interface Payload {
  id: string;
  created: number;
};

export interface Token {
  access_token: string;
  expired: number;
};