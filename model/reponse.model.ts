export interface BaseResponse <T> {
  status: number;
  message: string;
  data?: T;
  code?: number;
  dev_message?: string;
}

export class ResponseFactory {
  static ok<T>(data: T): BaseResponse<T> {
    return {
      status: 200,
      message: 'success',
      data: data
    }
  }
}