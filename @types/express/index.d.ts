import * as express from "express"

declare global {
  namespace Express {
    interface Request {
      user?: import('../../model/user.model').User;
    }
  }
}
