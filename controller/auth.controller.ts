import { User } from '../model/user.model';
import { Token, Payload } from '../model/token.model';
import { generateToken, comparePassword } from '../utils';
import { BadRequestError } from '../errors';

export namespace AuthController {
  export async function login(username: string, password: string): Promise<Token> {
    const user: User = await User.query().where({username: username}).first();
    if(user == null || !comparePassword(password, user.password)) 
      return Promise.reject(new BadRequestError('invalid username or password'));
    return Promise.resolve(generateToken({id: user.id, created: new Date().getTime()}));
  }
}