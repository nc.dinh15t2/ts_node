import { User } from '../model/user.model';
import { encodePassword } from '../utils';
import { v4 } from 'uuid';
import { QueryBuilder } from 'knex';
import { QueryBuilderType } from 'objection';

export async function register({username, email, password, name: {first, last}, gender}: User): Promise<User> {
  const encoded = await encodePassword(password);
  const newUser: User = await User.query().insertAndFetch({
    username,
    email,
    password: encoded,
    name: {first, last},
    gender,
    roles: ['admin', 'user']
  });
  delete newUser.password;
  return newUser;
};

interface GetAllUser {
  username?: string;
  email?: string;
  firstname?: string;
  lastname?: string;
}

const USER_SHORT_INFO: string[] = ['id','username','name'];
const USER_FULL_INFO: string[] = [...USER_SHORT_INFO, ...['email','roles','created']];
export async function getAll({username, email, firstname, lastname}: GetAllUser, authId: v4): Promise<User[]> {
  const query: QueryBuilderType<User> = User.query();
  if(username) query.where('username', 'like', `%${username.replace(/%/g,'\\%')}%`);
  if(email) query.where('email', 'like', `%${email}%`);
  const users: User[] = await query.select(...USER_SHORT_INFO).orderBy('username', 'asc');
  return users;
}

export async function getById(id: string, authId: string): Promise<User> {
  const user: User = await User.query().findById(id).select(...USER_FULL_INFO);
  return user;
}

interface UpdateUser {
  name?: {first?: string, last?: string}
}

export async function update(id: string, request: UpdateUser, authId: string): Promise<User> {
  const user: User = await User.query().findById(id);
  console.log(request);
  return user;
}