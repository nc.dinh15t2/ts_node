import { BaseResponse } from "./model/reponse.model";

export class BusinessError extends Error {
  status: number;
  code: number;
  devMessage: string;

  constructor(status: number, message: string, code: number, 
      devMessage: string) {
    super(message);
    this.status = status;
    this.code = code;
    this.devMessage = devMessage;
  }

  public getResponse(): BaseResponse<void> {
    const response = {
      status: this.status,
      message: this.message
    };
    if (process.env.SEV_MODE === 'test') 
      return {...response,...{code: this.code, dev_message: this.devMessage}};
    else return response;
  }
}

export class BadRequestError extends BusinessError {
  constructor(message: string, code: number = 9999, devMessage: string = 'No message') {
    super(400, message, code, devMessage);
  }
}

export class UnauthorizedError extends BusinessError {
  constructor() {
    super(401, 'Unauthorized', 1000, 'No message');
  }
}

export class ForbiddenError extends BusinessError {
  constructor() {
    super(403, 'Forbidden', 1001, 'No message');
  }
}